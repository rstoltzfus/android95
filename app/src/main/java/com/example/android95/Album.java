package com.example.android95;

import java.io.Serializable;
import java.util.ArrayList;

public class Album implements Comparable<Album>,Serializable{

    private static final long SerialVersionID = 1L;
    private String albumName;
    private ArrayList<Photo> album;

    public Album(String name){
        albumName = name;
        album = new ArrayList<Photo>();
    }

    public Album(String name, ArrayList<Photo> photos){
        albumName = name;
        album = photos;
    }

    public String getAlbumName(){ return albumName; }

    public void setAlbumName(String name){ albumName = name; }

    public ArrayList<Photo> getAlbum() { return album; }

    public void setAlbum(ArrayList<Photo> newAlbum){ album = newAlbum; }

    public void addPhoto(Photo photo){ album.add(photo); }

    public void removePhoto(Photo photo){ album.remove(photo); }

    public int getAlbumSize(){ return album.isEmpty() ? 0 : album.size(); }

    @Override
    public int compareTo(Album a){ return albumName.compareTo(a.albumName); }
}
