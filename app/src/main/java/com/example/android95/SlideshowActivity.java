package com.example.android95;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/*public class SlideshowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);

    }
}*/

import android.content.Context;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class SlideshowActivity extends AppCompatActivity {

    private Context context = this;
    private ViewFlipper viewFlipper;
    private float initialXPoint;
    private ArrayList<Photo> photos;
    private ArrayList<Bitmap> images;
    private int albumNum;
    private int startingImage;
    private File appDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);
        viewFlipper = findViewById(R.id.slideshowFlipper);
        appDir = getFilesDir();
        ArrayList<Album> albums = read();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            albumNum = extras.getInt("ALBUM_NUM");
            startingImage = extras.getInt("PHOTO_NUM");
        }
        photos = albums.get(albumNum).getAlbum();
        images = getThumbnails();
        for (Bitmap image : images) {
            ImageView imageView = new ImageView(SlideshowActivity.this);
            imageView.setImageBitmap(image);
            viewFlipper.addView(imageView);
        }
        viewFlipper.setDisplayedChild(startingImage);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialXPoint = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalx = event.getX();
                if (initialXPoint > finalx) {
                    if (viewFlipper.getDisplayedChild() == images.size()-1)
                        break;
                    viewFlipper.showNext();
                } else {
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }

    public ArrayList<Bitmap> getThumbnails(){
        ArrayList<Bitmap> loadedThumbnails = new ArrayList<>();
        if(photos != null) {
            for (Photo p : photos) {
                loadedThumbnails.add(BitmapFactory.decodeByteArray(p.getByteArray(), 0, p.getByteArray().length));
            }
        }
        return loadedThumbnails;
    }

    public ArrayList<Album> read() {
        File file = new File(appDir, MainActivity.FILENAME);
        ArrayList<Album> newAlbumList = null;
        try{
            if(file.length() == 0)
                file.createNewFile();
            else{
                FileInputStream fileInputStream = openFileInput(MainActivity.FILENAME);
                ObjectInputStream stream = new ObjectInputStream(fileInputStream);
                newAlbumList = (ArrayList<Album>) stream.readObject();
                fileInputStream.close();
                stream.close();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return newAlbumList;
    }
}