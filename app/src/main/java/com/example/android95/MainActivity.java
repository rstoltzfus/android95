package com.example.android95;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    public ArrayList<Album> albums;
    public File appDir;
    public static final String FILENAME = "AlbumList.txt";
    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;
    ListView listView;
    FloatingActionButton add;
    FloatingActionButton edit;
    FloatingActionButton delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appDir = getFilesDir();
        setContentView(R.layout.activity_main);

        add = findViewById(R.id.AddButton);
        edit = findViewById(R.id.EditButton);
        delete = findViewById(R.id.DeleteButton);
        listView = findViewById(R.id.ListView);

        albums = read();
        if(albums == null)
            albums = new ArrayList<>();

        final ArrayList<String> albumNames = getAlbumNames();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.activity_listview, albumNames);
        listView.setAdapter(arrayAdapter);

        //if someone holds down an album
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder extraOptions = new AlertDialog.Builder(context);
                extraOptions.setTitle("Options");

                //Delete currently selected item from list
                extraOptions.setPositiveButton("Delete", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        albumNames.remove(position);
                        albums.remove(position);
                        arrayAdapter.notifyDataSetChanged();
                        write(albums);
                    }
                });
                extraOptions.setNegativeButton("Rename", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlertDialog.Builder rename = new AlertDialog.Builder(context);
                        rename.setTitle("Rename Album");
                        final EditText input = new EditText(context);
                        input.setHint("Enter Album Name");
                        input.setInputType((InputType.TYPE_CLASS_TEXT));
                        rename.setView(input);

                        rename.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //check if anything inputted
                                if(input.getText().toString().isEmpty()) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                    alert.setTitle("Error");
                                    alert.setMessage("Error: Invalid Album Name.");
                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    alert.show();
                                }

                                //check if duplicates
                                else {
                                    boolean dup = false;
                                    for (int i = 0; i < albumNames.size(); i++) {
                                        if (albumNames.get(i).equalsIgnoreCase(input.getText().toString())) {
                                            dup = true;
                                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                            alert.setTitle("Error");
                                            alert.setMessage("Error: Duplicate Album Name.");
                                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            alert.show();
                                        }
                                    }
                                    if (!dup) {

                                        //creating new album
                                        albumNames.remove(position);
                                        albumNames.add(position,input.getText().toString());
                                        albums.get(position).setAlbumName(input.getText().toString());
                                        arrayAdapter.notifyDataSetChanged();
                                        write(albums);
                                    }
                                }

                            }
                        });
                        rename.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        });
                        rename.show();
                    }
                });
                extraOptions.show();
                return true;
            }
        });

        //If someone wants to open album
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
                intent.putExtra("USER_ALBUM_NUM", position);
                startActivity(intent);
            }
        });

        //Add Button clicked, adding new album
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder add = new AlertDialog.Builder(context);
                add.setTitle("Add new Album");
                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setHint("Enter Album Name");
                add.setView(input);
                add.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //check if anything inputted
                        if(input.getText().toString().isEmpty()) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                            alert.setTitle("Error");
                            alert.setMessage("Error: Invalid Album Name.");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.show();
                        }
                        else {
                            boolean dup = false;
                            //check if duplicates
                            for (int i = 0; i < albumNames.size(); i++) {
                                if (albumNames.get(i).equalsIgnoreCase(input.getText().toString())) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                    dup = true;
                                    alert.setTitle("Error");
                                    alert.setMessage("Error: Duplicate Album Name.");
                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    alert.show();
                                }
                            }
                            if(!dup) {
                                //creating new album
                                Album newAlbum = new Album(input.getText().toString());
                                albums.add(newAlbum);
                                albumNames.add(input.getText().toString());
                                arrayAdapter.notifyDataSetChanged();
                                write(albums);
                            }
                        }
                    }
                });
                add.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                add.show();
            }
        });
    }
    
    public ArrayList<Album> read() {
        File file = new File(appDir, FILENAME);
        ArrayList<Album> newAlbumList = null;
        try{
            if(file.length() == 0)
                file.createNewFile();
            else{
                fileInputStream = openFileInput(FILENAME);
                ObjectInputStream stream = new ObjectInputStream(fileInputStream);
                newAlbumList = (ArrayList<Album>) stream.readObject();
                fileInputStream.close();
                stream.close();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return newAlbumList;
    }

    public void write(ArrayList<Album> albumList){
        try{
            fileOutputStream = openFileOutput(FILENAME, MODE_PRIVATE);
            ObjectOutputStream stream = new ObjectOutputStream(fileOutputStream);
            stream.writeObject(albumList);
            fileOutputStream.close();
            stream.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAlbumNames() {
        ArrayList<String> albumNames = new ArrayList<>();
        for(Album a: albums){
            albumNames.add(a.getAlbumName());
        }
        return albumNames;
    }
}
