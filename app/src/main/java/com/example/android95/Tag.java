package com.example.android95;

import java.io.Serializable;

public class Tag implements Serializable {
    public static final long SerialVersionID = 1L;
    private String tagName;
    private String tagValue;

    public Tag(String name, String value){
        tagName = name;
        tagValue = value;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

    public boolean equals(Tag tag){
        if (tag.getTagName().equalsIgnoreCase(tagName) && tag.getTagValue().equalsIgnoreCase(tagValue))
            return true;
        return false;
    }
    public String toString(){
        return "(" + tagName + ":" + tagValue + ")";
    }
}
