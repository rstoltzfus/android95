package com.example.android95;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoActivity extends AppCompatActivity {

    final Context context = this;
    public final String FILENAME = "AlbumList.txt";
    ArrayList<Album> albums;
    Album thisAlbum;
    ArrayList<Photo> photos;
    ArrayList<Bitmap> thumbnails;
    int albumNum;
    GridView grid;
    ThumbNailAdapter thumbNailAdapter;
    File appDir;
    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;
    FloatingActionButton add;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        appDir = getFilesDir();
        albums = read();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            albumNum = extras.getInt("USER_ALBUM_NUM");
            thisAlbum = albums.get(albumNum);
            photos = thisAlbum.getAlbum();
            thumbnails = getThumbnails();
        } else {
            thisAlbum = null;
            photos = new ArrayList<>();
        }
        title = findViewById(R.id.TitleText);
        title.setText(thisAlbum.getAlbumName());

        add = findViewById(R.id.AddButton);
        grid = findViewById(R.id.Grid);

        thumbNailAdapter = new ThumbNailAdapter(context, thumbnails);
        grid.setAdapter(thumbNailAdapter);

        //Clicked Add button, adding new photo
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent,1);
            }
        });

        Button search = findViewById(R.id.searchButton);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder search = new AlertDialog.Builder(context);
                search.setTitle("Add New Tag");
                final EditText inputType = new EditText(context);
                inputType.setHint("Tag Type");
                final EditText inputValue = new EditText(context);
                inputValue.setHint("Tag Value");
                inputType.setInputType((InputType.TYPE_CLASS_TEXT));
                inputValue.setInputType((InputType.TYPE_CLASS_TEXT));
                LinearLayout searchLayout = new LinearLayout(context);

                searchLayout.addView(inputType);
                searchLayout.addView(inputValue);
                search.setView(searchLayout);
                search.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(inputType.getText().toString().equalsIgnoreCase("location") || inputType.getText().toString().equalsIgnoreCase("person")){

                            Album searchResult = new Album(inputType.getText().toString() + ":" + inputValue.getText().toString());
                            for (Photo p: photos){
                                for (Tag t: p.getTags()){
                                    if (inputType.getText().toString().equalsIgnoreCase(t.getTagName()) && t.getTagValue().contains(inputValue.getText().toString())){
                                        searchResult.getAlbum().add(p);
                                        break;
                                    }
                                }
                            }
                            if (searchResult.getAlbum().size() == 0){
                                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                alert.setTitle("Error");
                                alert.setMessage("Error: Nothing Fits Search Parameters.");
                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alert.show();
                            }
                            else{
                                Intent intent = new Intent(PhotoActivity.this, PhotoActivity.class);
                                intent.putExtra("album", searchResult);
                                albums.add(searchResult);
                                write(albums);
                                intent.putExtra("USER_ALBUM_NUM", albums.size()-1);
                                startActivity(intent);
                            }
                        }
                        else{
                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                            alert.setTitle("Error");
                            alert.setMessage("Error: Invalid Tag Type.");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.show();
                        }
                    }
                });
                search.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(PhotoActivity.this, PhotoActivity.class);
                        intent.putExtra("USER_ALBUM_NUM", albumNum);
                        startActivity(intent);
                    }
                });
                search.show();
            }
        });
    }

    public boolean showExtraOptions(View view, final int position){
        final Dialog Options = new Dialog(context);
        final View layout = View.inflate(context,R.layout.options, null);
        Options.setContentView(layout);

        Button remove = layout.findViewById(R.id.RemoveButton);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //need to update the saved objects

                photos.remove(position);
                for(int j = 0; j < albums.size(); j++){
                    if(albums.get(j).getAlbumName().equalsIgnoreCase(thisAlbum.getAlbumName())){
                        albums.remove(j);
                        albums.add(j, thisAlbum);
                    }
                }
                write(albums);
                thumbnails.remove(position);
                thumbNailAdapter.notifyDataSetChanged();
                Options.dismiss();;

            }
        });
        Button slideshow = layout.findViewById(R.id.SlideshowButton);
        final int position2 = position;
        slideshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PhotoActivity.this, SlideshowActivity.class);
                intent.putExtra("ALBUM_NUM", albumNum);
                intent.putExtra("PHOTO_NUM", position2);
                startActivity(intent);
            }
        });


        Button move = layout.findViewById(R.id.MoveButton);
        move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder move = new AlertDialog.Builder(context);
                move.setTitle("Move Photo");
                move.setMessage("Destination: ");
                final EditText input = new EditText(context);
                input.setHint("Enter Album Name");
                move.setView(input);
                move.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int j, k;
                        if(input.getText().toString().equalsIgnoreCase(albums.get(albumNum).getAlbumName())){
                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                            alert.setTitle("Error");
                            alert.setMessage("Error: Destination Album Same as Current Album.");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }
                        for(j = 0; j < albums.size(); j++){
                            if(albums.get(j).getAlbumName().equalsIgnoreCase(input.getText().toString())){
                                albums.get(j).addPhoto(photos.remove(position));
                                write(albums);
                                thumbnails.remove(position);
                                thumbNailAdapter.notifyDataSetChanged();
                                break;
                            }
                        }
                        if(j == albums.size() && !albums.get(j).getAlbumName().equalsIgnoreCase(input.getText().toString())){
                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                            alert.setTitle("Error");
                            alert.setMessage("Error: Destination Album not found.");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.show();
                        }
                        Options.dismiss();


                    }
                });
                move.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(PhotoActivity.this, PhotoActivity.class);
                        intent.putExtra("USER_ALBUM_NUM", albumNum);
                        startActivity(intent);
                    }
                });
                move.show();
            }
        });

        Options.show();
        return false;   //pretty sure this should be true
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                //returning from image selection
                case 1: {
                    Uri selectedPhotoUri = data.getData();
                    String photoPath = selectedPhotoUri.getPath();
                    String photoName;
                    Bitmap thisThumbnail = null;
                    int cut = photoPath.lastIndexOf('/');
                    if (cut != -1) {
                        photoName = photoPath.substring(cut + 1);
                        photoName.concat(".jpg");
                    } else {
                        photoName = "new_photo.jpg";
                    }
                    Photo thisPhoto = new Photo(photoName);
                    try {
                        thisThumbnail = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedPhotoUri));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (thisThumbnail != null) {
                        thumbnails.add(thisThumbnail);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thisThumbnail.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                        thisPhoto.setByteArray(stream.toByteArray());
                    }

                    photos.add(thisPhoto);
                    thisAlbum.setAlbum(photos);
                    albums.set(albumNum, thisAlbum);
                    write(albums);
                    thumbNailAdapter.notifyDataSetChanged();
                    break;
                }
                default: break;
            }
        }
    }

    public class ThumbNailAdapter extends ArrayAdapter {
        private Context context;
        private ArrayList<Bitmap> thumbnails;

        ThumbNailAdapter(Context context, ArrayList<Bitmap> thumbnails){
            super(context,R.layout.image_display,thumbnails);
            this.context = context;
            this.thumbnails = thumbnails;
        }

        @Override
        public View getView(int position, View currentView, @NonNull ViewGroup parent){

            if (currentView == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                currentView =  inflater.inflate(R.layout.display, parent, false);
            }
            ImageView imageView = currentView.findViewById(R.id.imageView);
            imageView.setImageBitmap(thumbnails.get(position));
            final int position2 = position;

            //Show extra options
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return showExtraOptions(v, position2);
                }
            });

            //Display
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog options = new Dialog(context);

                    final View layout1 = View.inflate(context,R.layout.image_display,null);
                    final Photo selected = photos.get(position2);
                    String tagNames = "";
                    for (Tag t: selected.getTags()){
                        tagNames += t.toString() + " , ";
                    }
                    if (tagNames.length() != 0)
                        tagNames.substring(0,tagNames.length()-4);

                    ((TextView)layout1.findViewById(R.id.textView)).setText(tagNames);
                    Bitmap b = BitmapFactory.decodeByteArray(selected.getByteArray(), 0, selected.getByteArray().length);
                    ((ImageView)layout1.findViewById(R.id.imageView)).setImageBitmap(b);

                    options.setContentView(layout1);
                    Button addTag = layout1.findViewById(R.id.AddTagButton);
                    addTag.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder add = new AlertDialog.Builder(context);
                            add.setTitle("Add New Tag");
                            LinearLayout addLayout = new LinearLayout(context);
                            addLayout.setOrientation(LinearLayout.VERTICAL);
                            final EditText inputType = new EditText(context);
                            inputType.setHint("Tag Type");
                            final EditText inputValue = new EditText(context);
                            inputValue.setHint("Tag Value");
                            inputType.setInputType((InputType.TYPE_CLASS_TEXT));
                            inputValue.setInputType((InputType.TYPE_CLASS_TEXT));
                            addLayout.addView(inputType);
                            addLayout.addView(inputValue);
                            add.setView(addLayout);
                            add.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!inputType.getText().toString().equalsIgnoreCase("location") && !inputType.getText().toString().equalsIgnoreCase("person")) {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                        alert.setTitle("Error");
                                        alert.setMessage("Error: Invalid Tag Type.");
                                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                        alert.show();
                                    }
                                    else{
                                        boolean dup = false;
                                        for(Tag t: selected.getTags()){
                                            if(t.getTagName().trim().equalsIgnoreCase(inputType.getText().toString().trim()) && t.getTagValue().trim().equalsIgnoreCase(inputValue.getText().toString().trim())){
                                                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                                alert.setTitle("Error");
                                                dup = true;
                                                alert.setMessage("Error: Duplicate Tag.");
                                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                alert.show();
                                            }
                                        }
                                        if(!dup){
                                            Tag newTag = new Tag(inputType.getText().toString(),inputValue.getText().toString());
                                            selected.getTags().add(newTag);
                                            for(int i = 0; i < photos.size(); i++){
                                                if(photos.get(i).getName().equalsIgnoreCase(selected.getName())){
                                                    photos.remove(i);
                                                    photos.add(selected);
                                                    break;
                                                }
                                            }
                                            for(int i = 0; i < albums.size(); i++){
                                                if(albums.get(i).getAlbumName().equalsIgnoreCase(thisAlbum.getAlbumName())){
                                                    albums.remove(i);
                                                    albums.add(i, thisAlbum);
                                                    break;
                                                }
                                            }
                                            write(albums);
                                        }
                                    }
                                }
                            });
                            add.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(PhotoActivity.this, PhotoActivity.class);
                                    intent.putExtra("USER_ALBUM_NUM", albumNum);
                                    startActivity(intent);
                                }
                            });
                            options.dismiss();
                            add.show();

                        }

                    });
                    Button deleteTag = layout1.findViewById(R.id.DeleteTagButton);
                    deleteTag.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for(Tag t: selected.getTags()){
                                AlertDialog.Builder delete = new AlertDialog.Builder(context);
                                delete.setTitle("Delete Tag");
                                final EditText inputType = new EditText(context);
                                inputType.setHint("Tag Type");
                                final EditText inputValue = new EditText(context);
                                inputValue.setHint("Tag Value");
                                inputType.setInputType((InputType.TYPE_CLASS_TEXT));
                                inputValue.setInputType((InputType.TYPE_CLASS_TEXT));
                                LinearLayout deleteLayout = new LinearLayout(context);
                                deleteLayout.setOrientation(LinearLayout.VERTICAL);
                                deleteLayout.addView(inputType);
                                deleteLayout.addView(inputValue);
                                delete.setView(deleteLayout);
                                delete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PhotoActivity.this, PhotoActivity.class);
                                        intent.putExtra("USER_ALBUM_NUM", albumNum);
                                        startActivity(intent);
                                    }
                                });
                                delete.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (inputType.getText().toString().equalsIgnoreCase("location") || inputType.getText().toString().equalsIgnoreCase("person")){
                                            if(!inputValue.getText().toString().equalsIgnoreCase("")) {
                                                selected.deleteTag(new Tag(inputType.getText().toString(), inputValue.getText().toString()));
                                                for(int j = 0; j < photos.size(); j++){
                                                    if(photos.get(j).getName().equalsIgnoreCase(selected.getName())){
                                                        photos.remove(j);
                                                        photos.add(selected);
                                                    }
                                                }
                                                for(int j = 0; j < albums.size(); j++){
                                                    if(albums.get(j).getAlbumName().equalsIgnoreCase(thisAlbum.getAlbumName())){
                                                        albums.remove(j);
                                                        albums.add(j, thisAlbum);
                                                    }
                                                }
                                                write(albums);
                                            }
                                            else{
                                                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                                alert.setTitle("Error");
                                                alert.setMessage("Error: No Tag Value.");
                                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                alert.show();
                                            }
                                        }
                                        else{
                                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                            alert.setTitle("Error");
                                            alert.setMessage("Error: Invalid Tag Type.");
                                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            alert.show();
                                        }
                                    }
                                });
                                options.dismiss();
                                delete.show();
                            }
                        }
                    });
                    options.show();
                }
            });
            return currentView;
        }
    }

    public ArrayList<Bitmap> getThumbnails(){
        ArrayList<Bitmap> loadedThumbnails = new ArrayList<>();
        if(photos != null) {
            for (Photo p : photos) {
                loadedThumbnails.add(BitmapFactory.decodeByteArray(p.getByteArray(), 0, p.getByteArray().length));
            }
        }
        return loadedThumbnails;
    }

    public ArrayList<Album> read() {
        File file = new File(appDir, FILENAME);
        ArrayList<Album> newAlbumList = null;
        try{
            if(file.length() == 0)
                file.createNewFile();
            else{
                fileInputStream = openFileInput(FILENAME);
                ObjectInputStream stream = new ObjectInputStream(fileInputStream);
                newAlbumList = (ArrayList<Album>) stream.readObject();
                fileInputStream.close();
                stream.close();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return newAlbumList;
    }

    public void write(ArrayList<Album> albumList){
        try{
            fileOutputStream = openFileOutput(FILENAME, MODE_PRIVATE);
            ObjectOutputStream stream = new ObjectOutputStream(fileOutputStream);
            stream.writeObject(albumList);
            fileOutputStream.close();
            stream.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
