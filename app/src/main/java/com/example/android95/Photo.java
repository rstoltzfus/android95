package com.example.android95;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable {

    private static final long serialVersionID = 1L;
    private String name;
    private ArrayList<Tag> tags;
    private byte[] byteArray;

    Photo(String n){
        name = n;
        tags = new ArrayList<>();
    }

    public boolean addTag(Tag tag){
        for(Tag t: tags){
            if(t.getTagName().equalsIgnoreCase(tag.getTagName()) && t.getTagValue().equalsIgnoreCase(tag.getTagValue()))
                return false;
        }
        tags.add(tag);
        return true;
    }

    public void deleteTag(Tag tag){
        for(Tag t: tags){
            if(t.getTagName().equalsIgnoreCase(tag.getTagName()) && t.getTagValue().equalsIgnoreCase(tag.getTagValue())) {
                tags.remove(t);
                return;
            }

        }
    }

    public String getName(){ return name; }

    public ArrayList<Tag> getTags() { return tags; }

    public void setName(String n){ name = n; }

    public void setByteArray(byte[] b) { byteArray = b; }

    public byte[] getByteArray() { return byteArray; }

}
